This paper intended to show how to solve localization problem in known map by using Adaptive Monte Carlo Localization in Gazebo simulation environment. Common used filters for localization such as Kalman Filter and Particle Filter will be compared. Which ROS packages with which parameters used and how to set them for achieving better result will be discussed.

To start Personal Model:
In catkin_ws folder

roslaunch udacity_bot my_world.launch

roslaunch udacity_bot amcl.launch

rosrun udacity_bot navigation_goal 

To start Benchmark Model:
In catkin_ws folder

roslaunch udacity_bot udacity_world.launch

roslaunch udacity_bot amcl.launch

rosrun udacity_bot navigation_goal 